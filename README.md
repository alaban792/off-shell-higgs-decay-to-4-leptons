# Off-shell Higgs decay to 4 leptons



## Alan Hernández FESC-UNAM, February 2024

This repository contains the anlytical expression for the polarized and unpolarized partial width of the $H^\ast\rightarrow ZZ\rightarrow\overline{\ell}_1\ell_1\overline{\ell}_2\ell_2$ process described in https://arxiv.org/abs/2402.18497.
## Aditional anomalous couplings
The analytical expression for $\hat{b}_Z$ can be found in my ZZH Anomalous Couplings project (https://gitlab.com/fcfm-buap-rc-group/zzh-anomalous-couplings)


## Authors and acknowledgment
Please cite https://arxiv.org/abs/2301.13127 and https://arxiv.org/abs/2402.18497  if our repository  is used in your research.

## Support and comments
For any questions, please contact Alan I. Hernández (alaban7_3@hotmail) FESC-UNAM Radiative Corrections Group leader, February 2024.

